import React, { useContext } from 'react';
import { AppContext } from './UsestateComp';

const Child = () => {
    const{setCount} = useContext(AppContext)
    return (
        <div >
            <button style={{height:"50px",width:"100px"}}  onClick={(e)=>{setCount(e.target.value++)}}>Click</button>
        </div>
    );
};

export default Child;