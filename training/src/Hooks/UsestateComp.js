import React,{useState, useEffect, useMemo, createContext} from 'react';

import Child from './Child';
import ChildA from './ChildA';

export const AppContext = createContext(null);
// const UsestateComp = () => {
//     // const [count,Setcount] = useState(0);
//     // return (
//     //     <div>
//     //         <h1>Count is {count}</h1>
//     //         <button type="button" onClick={()=>{Setcount(count+1)}}>Increment count
            
//     //         </button>
//     //     </div>
//     // );
//     // const [username,setUsername] = useState("");
//     // const [password,setPassword] = useState("");
//     // return(
//     //     <form>
//     //         <label>
//     //             Username:
//     //             <input value={username} name="username" type="text" onChange={(event)=>setUsername(event.target.value)}/>
//     //         </label>
//     //         <label>
//     //              Password:
//     //             <input value={password} name="password" type="password" onChange={(event)=>setPassword(event.target.value)}/>
//     //         </label><br></br>
//     //         <button>submit</button>
//     //     </form>)
//     // const[user,setUser] = useState(0);
//     // const[calculation,setCalculation] = useState(0);
//     // const url =""
//     // const fetchData = async() =>{
//     //     const data = await fetch(url);
//     //     const json = await data.json()
//     //     setUser(json)
//     // }

//     // useEffect(()=>{

//     //     fetchData();

//     //setCount(() => count+1)
//     // },[])
//     //return<h1>I have rendered {count} times</h1>

//     //return<div>Current user:<pre>{JSON.stringify(user,null,2)}</pre></div>
const UsestateComp = () => {
//const [username, setUsername] = useState("Abhinav Prabhakar");
const [count,setCount] = useState(0);
     return(
         <AppContext.Provider value={{count,setCount}}>
             <Child/>
             <ChildA/>
         </AppContext.Provider>
     )
 };

 export default UsestateComp;


