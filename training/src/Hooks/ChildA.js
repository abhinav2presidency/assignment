import React,{useContext} from 'react';
import { AppContext } from './UsestateComp';
const ChildA = () => {
    const {count} = useContext(AppContext)
    return (
        <div>
            <h1>Counter is:{(count)}</h1>
        </div>
    );
};

export default ChildA;