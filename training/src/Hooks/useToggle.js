import { useState } from 'react';

// const useToggle = (defaultValue) => {
//    const[value,setValue]  = useState(defaultValue)
//    function toggleValue(value){
//        setValue((currentvalue)=>
//            typeof value == "boolean" ? value : !currentvalue
//        );
//    } 
//    return [value,toggleValue];
// };

//export default useToggle;


const useClick = (defaultValue) => {
    const [count,setCount] = useState(defaultValue);
    const Increment = () =>{
        return setCount(count+1)
    };
    const Decrement = () =>{
        return setCount(count-1)
    } 
    return [ count,Increment,Decrement];
};

export default useClick;