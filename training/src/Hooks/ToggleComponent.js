import React from 'react';
import useClick from './useToggle';

const ToggleComponent = () => {
    const[count, Increment, Decrement] = useClick(0);
    return (
        <div>{count}
        <button onClick={Increment}>Up</button>
        <button onClick={Decrement}>Down</button>
        </div>
    );
};

export default ToggleComponent;