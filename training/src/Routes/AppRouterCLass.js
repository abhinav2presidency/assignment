import { BrowserRouter,Routes, Route } from 'react-router-dom';
import '../App.css';
import AboutUs from '../pages/AboutUs';
import Contact from '../pages/Contact';
import Layout from '../pages/Layout';
import Login from '../pages/Login';

export default function AppRouter()
{return(
  <BrowserRouter>
    <Routes>
      {/* <Route path ="/login" element={<Login/>} /> */}
      <Route path="/" element={<Layout/>}/>
      <Route index element={<Login/>}/>
      <Route path ="aboutus" element={<AboutUs/>} />
      <Route path ="contact" element={<Contact/>} />
    </Routes>
  </BrowserRouter>
)
}

