import './App.css';
import AppRouter from './Routes/AppRouter';
import Parent from './Parent';
import UsestateComp from './Hooks/UsestateComp';
import useToggle from './Hooks/useToggle';
import ToggleComponent from './Hooks/ToggleComponent';
import Signin from './Signin';
import Login from './pages/Login';

function App() {
  return (
    <div className="App">
      <header>
        {/* <ToggleComponent/> */}
        {/* <UsestateComp/> */}
        {/* <Parent/> */}
        {/* <Signin/> */}
        {/* <AppRouter/> */}
      </header>
    </div>
  );
}

export default App;
