import { useState } from "react";
import React from "react";
import './App.css';

const Signin = () => {
    const[formData,setFormData]=useState({email:'',password:''});
    const [email,setEmail] = useState("PWC");
    const[password,setPassword]=useState("1234");
    const submit = () => {
        console.log("Button is clicked");
    }
    const handleChange = (event) =>{
        //console.log("Name-", event.target.name);
        //console.log("value-", event.target.value);
        /*if(event.target.name === "email")
            setEmail(event.target.value);
        else
            setPassword(event.targe.value);*/
        setFormData({[event.target.name]:event.target.value})
        console.log(formData)
    }
    const handleChangeForgot=()=>{

    }
    
    return(
        <div>
            <h1 className = "signin" >Sign in</h1>
            <form action="" >
                <div className="Email" >
                    <label htmlFor="email">Email address</label><br></br>
                    <input type = "text" name = "email" id="email" className="holder" placeholder="Enter email" onChange={handleChange} value={formData.email}></input>
                    <p>Email:{email}</p>
                </div>
                <div className="pass">
                    <label htmlFor="pass">Password</label><br></br>
                    <input type = "password" name = "password" id="pass" className="holder" placeholder="Enter password" onChange={handleChange} value ={formData.password}></input>
                    
                    <p>Password:{password}</p>
                    
                </div >
                <input type="button" className="login-submit" onClick={submit} value="Submit"></input>
                <div >
                    <label style={{color:"black"}}>Forgot</label>
                    <input type="submit" className="App-link" value="password?"></input>
                    {/* <input type = "password" name = "forgot-password" placeholder="Enter password" onChange={handleChange} value ={formData.password}></input>           */}
                </div>
            </form>
        </div>
    ) 
}
export default Signin