import { useState } from "react";

const Child = (props)=>{
    return(<>  
        <div style={{border:"1px solid black",width:"400px",height:"150px",padding:"20px",color:"black",backgroundColor:"bisque",marginLeft:"70px",textAlign:"center",marginTop:"70px"}}>
        <p>Name:{props.info.name}</p>
        <p>Phone:{props.info.phone}</p>
        <p>Address:{props.info.addr}</p>
        </div>

        <div style={{border:"1px solid black",width:"400px",height:"150px",padding:"20px",color:"black",backgroundColor:"bisque",marginLeft:"70px",textAlign:"center",marginTop:"30px"}}>
        <p>Name:{props.info.name2}</p>
        <p>Phone:{props.info.phone2}</p>
        <p>Address:{props.info.addr2}</p>
        </div>

    </>)
}

export default Child;
