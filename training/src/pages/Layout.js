import { Outlet, Link } from "react-router-dom";
import "../App.css";

const Layout = () => {
  return (
    <>
      <nav className="layout">
        <div>
            <a  href="/" style={{color:"white", textDecoration: "none", paddingRight:"200px" }}>Login</a>
            <Link to="/aboutus" style={{color:"white", textDecoration: "none"}}>About</Link>
            <Link to="/contact"style={{color:"white", textDecoration: "none", paddingLeft:"200px"}}>Contact</Link>
        </div>
      </nav>

      <Outlet />
    </>
  )
};

export default Layout;