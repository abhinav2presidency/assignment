import { configureStore, combineReducers} from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import { watcherSagaOne } from "../container/container-one/saga/containerOneSaga"
import containerOneReducer from "../container/container-one/redux/containerOneSlice"; 

const sagaMiddleware = createSagaMiddleware();

const reducer = combineReducers({
    containerOne: containerOneReducer
}); 

const store = configureStore({
    reducer, 
    middleware: [sagaMiddleware],
});

sagaMiddleware.run(watcherSagaOne)
export default store;