import './App.css';
import React from 'react';
import CustomForm from './container/container-one/form';

function App() {
  return (
    <div className="App">
      <CustomForm/>
    </div>
  );
}

export default App;
