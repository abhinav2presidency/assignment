import {call,put} from "redux-saga/effects";
import {requestDataOne } from "./containerOneRequest";
import { setServerData } from "./containerOneSlice";

export function* ContainerOneHandler(action) {
    try{
        const response = yield call(requestDataOne);
        yield put(setServerData({...response}));

    } catch(err) {
        console.log(err);
    }
}