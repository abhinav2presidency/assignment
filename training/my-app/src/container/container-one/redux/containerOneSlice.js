import {createSlice} from "@reduxjs/toolkit"

const containerOneSlice = createSlice({
    name: "containerOne",
    initialState: {
        inputValue: "From slice, initial value",
        errorValue: "Not an error",
        serverData: [],
    },
    reducers: {
    getContainerData() {},
    setContainerData(state,action) {
        console.log("action", action);
        const {inputValue }= action.payload;
        return{ ...state, inputValue};
    },
    getServerData(){},
    setServerData(state, action){
        console.log("action=", action)
        const{data} = action.payload
        return {...state, serverData: data}
    }    
    }
       
    
});

export const { getContainerData, setContainerData,getServerData,setServerData } = containerOneSlice.actions;
export default containerOneSlice.reducer;
