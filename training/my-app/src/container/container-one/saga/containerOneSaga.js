import {takeLatest } from "redux-saga/effects"
import {ContainerOneHandler} from "../redux/containerOneHandlers"
import { getServerData } from "../redux/containerOneSlice"

export function* watcherSagaOne(){
    yield takeLatest(getServerData.type, ContainerOneHandler);
}