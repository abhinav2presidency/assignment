import React, { useEffect, useState } from 'react';
import {useDispatch, useSelector} from "react-redux"
import { getServerData , setContainerData} from '../redux/containerOneSlice';

const CustomForm = () => { 
    const dispatch = useDispatch();
    
    useEffect(() => {
        dispatch(getServerData({}))
    }, [])

    const [inputValue, setInputValue] = useState("");

    const{inputValue: originalValue, errorValue, serverData} = useSelector(
        (state) => state.containerOne
    )

    console.log("From form component::", serverData)

    const handleCLick = () =>{
        console.log("inputvalue = " , inputValue);
    }

    return (
        <>  
            <h3>{originalValue}</h3>
            <h4>{errorValue}</h4>
            <input type ="text" value={inputValue} onChange={(e) => setInputValue(e.target.value)} />
            <input type ="button" value="submit" onClick={handleCLick} />
            <div>
                <table>
                    <thead>
                        <tr>
                        <th>ID</th>
                        <th>Title</th>
                        </tr>
                    </thead>
                    <tbody>
                        {serverData.map((tableData,id)=>{
                            return(
                                <tr key={id}>
                                    <td>{tableData.id}</td>
                                    <td>{tableData.title}</td>
                                </tr>
                            )
                        })}
                    </tbody>
 
                </table>
            </div>
        </>
    );
};

export default CustomForm;