
From Gitlab copy above 

OR

git init
git add .
git commit -m "Push existing project to GitLab"
git remote add source https://gitlab.com/cameronmcnz/example-website.git
git push -u -f source master

git config http.sslVerify false -> for disabling ssl

"Settings" → "Repository" → "Expand" -> "unprotect branch" -> to force push

Remove git: rmdir /s .git

moving branches : git checkout <branch name>

seeing all branches : git branch -a
making branch : git branch <branch name>
rename branch : git branch -m old-name new-name